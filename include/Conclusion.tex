\chapter{Conclusion}

In this case study, we have researched the feasibility of realizing scalable
conflict-resolution for a configuration system as large as the Linux kernel
configurator. In academia, we found the algorithm RangeFix, which had an
existing implementation in Scala. By integrating this implementation of
RangeFix with the Linux kernel configurator xconfig, we evaluated its
suitability for resolving configuration conflicts. Through a survey, which
Linux developers and users took part in, we evaluated whether such a system
would be beneficial. We also studied how the algorithm could be implemented in
C, utilizing a SAT-based constraint solver, which would achieve greater
community acceptance.

The existing Scala implementation of RangeFix, we found to be unsuitable in its
current shape to be integrated into the Linux kernel configurators. To start
with, it is highly unlikely that a non-C solution to the problem will ever be
accepted into the mainline Linux kernel branch. Especially a solution written
in Scala, which requires one to install a 70 MB OpenJDK binary and a 110 MB
Scala binary. Furthermore, the source code of the Scala implementation of
RangeFix has not been maintained for quite some time, and compiling it requires
old versions of Java and Scala. These old dependencies are not so easily
available on a modern Linux distribution. The existing Scala implementation
also depends on an SMT solver, however, the Linux community would prefer to
base a solution on a SAT solver. The source code would also need to be further
polished. It is currently only implemented as a proof of concept within a
research project setting, and is therefore lacking in documentation and coding
standards. It is also lacking in features, for instance, it is only possible to
resolve unmet dependencies with inputs of the type tristate, not even boolean
inputs are currently supported. In our evaluation, we also found the
implementation to generally perform worse than the users' expectations.
Furthermore, the running time is also irregular, and for some configuration
options it may run for a very long time. Since we have to run the Scala
implementation of RangeFix as an external program in xconfig, it also poses a
challenge in C when its returned fixes, which can possibly contain complex
expressions, must be parsed as strings. The existing Scala implementation of
RangeFix works very well for demonstrating the capabilities of the algorithm,
but it is not production-ready, which neither was its authors' intention.

Our C implementation of the RangeFix algorithm is neither production-ready.
Only the first stage of the algorithm has been implemented, which means that it
is only able to generate diagnoses, but not fixes. We based our work on
Satconfig, which translates Kconfig models into SAT problems. However,
Satconfig was found to contain deficiencies, such as not handling tristates in
dependency expressions correctly. The performance did also suffer from the same
irregularities as the Scala implementation did, which might be due to
complexities in the Kconfig model or a case that the RangeFix algorithm does
not handle well. However, even if our C implementation of RangeFix is lacking
in several regards, it did prove the principle. It is possible to implement
scalable conflict-resolution support for a software system as large as the
Linux kernel. The Linux community's requirements were respected, and the
RangeFix algorithm appears to work well for conflict-resolution. However, our
implementation would need more development before it would be acceptable into
the mainline kernel repository.

A reoccurring problem is the many edge cases that exist in the Kconfig
language. What the language lacks, is a complete specification. This explains
why implemented parsers of the language do commonly suffer from deficiencies.
In this thesis, we have observed that the existing implementation of RangeFix
did not correctly handle assignment of values to configuration options without
prompts, and Satconfig got tristates wrong. But other tools, such as
Undertaker, Kconfigreader and LVAT, have also been found to make various
errors~\cite{el2015analysing}.

To solve the problems inherent in implementing Kconfig language parsers and
feature modeling tools, the language would likely need to be refined. A
parallel can be drawn to the markup language Markdown, which has a plethora of
community implementations with subtle differences. That sparked the initiative
CommonMark~\cite{commonmark}, which attempts to unify the Markdown
implementations by providing an extensive specification, examples, a reference
implementation, and a test suite. A similar initiative for the Kconfig language
would probably be healthy. Another suggestion is to simplify the Kconfig
language, and design it for being more easily translated into logic
propositional formulas that can be used by reasoners. Rather than trying to
bolt advanced translations of Kconfig on top the language and the kernel's
Kconfig infrastructure, it is probably wiser to design the language from the
ground up for being more easily compatible with reasoners.
