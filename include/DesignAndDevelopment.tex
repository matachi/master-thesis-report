\chapter{Design and development}
\label{design_and_development}

Two artifacts have been produced to investigate the feasibility of realizing
support for interactive conflict-resolution in the Kconfig configurator
xconfig. Both are versions of the Kconfig configurator xconfig, but they
utilize different implementations of RangeFix for assisting the user in
satisfying unmet dependencies. One of them utilizes the existing Scala
implementation of RangeFix, while the other utilizes our implementation of
RangeFix in C that depends on a SAT solver.

The first five sections of this chapter describe design elements that make up
our partial implementation of RangeFix in C. Together, they implement the first
stage of the RangeFix algorithm. In Section~\ref{encode_kconfig_as_sat}, the
encoding of a Kconfig model as a SAT problem is described. Next, in
Section~\ref{read_and_set_configuration}, how the user's configuration is read
and translated into soft constraints in the SAT problem is explained. In
Section~\ref{generate_unsatisfiable_cores_sat}, we take a look at how
unsatisfiable cores are generated in the existing Scala implementation of
RangeFix with an SMT solver, and how we achieve the same thing in C with a SAT
solver. These sections are tied together in Section~\ref{generate_diagnoses},
where we explain how we generate the diagnoses for the first stage of the
RangeFix algorithm. Lastly, in Section~\ref{simplify_diagnoses}, we explain why
and how we simplify the diagnoses by removing redundant configuration options.

The last section, Section~\ref{integrate_with_xconfig}, describes how both
implementations have been integrated with xconfig. The Linux configurators are
not engineered for letting the user cause conflicts, which meant that some
compromises in the design had to be made. Furthermore, since the codebase for
the existing Scala implementation has not been maintained for a long time, some
further compromises had to be made with regards to the utilized Linux kernel
versions.

\section{Encode a Kconfig model as a SAT problem}
\label{encode_kconfig_as_sat}

In this section, our encoding of a Kconfig model as a SAT problem is presented.
This encoding is to a large extent based on the encoding found in
Satconfig~\cite{satconfig}, which is unpublished and to a large extent
undocumented. The encoding can be divided into two separate phases. First, the
configuration options, declared in the Kconfig model, are translated into
boolean literals in the SAT problem. Next, the constraints imposed by the
various Kconfig attributes are translated into CNF clauses in the SAT problem.

\subsection{Configuration option encoding}

The two configuration option types to consider are \texttt{bool} and
\texttt{tristate}. The value of a configuration option with the type
\texttt{bool} is simply encoded using a single literal. However, a
configuration option with the type \texttt{tristate} has three states, and
therefore needs two literals to encode its value. How the \texttt{tristate}'s
states are encoded using two literals is depicted in Table~\ref{fig:tristate}.

\begin{table}
  \centering
  \caption{Encoding a \texttt{tristate} with two literals.}
  \begin{tabular}{|l|l|l|}
    \hline
    \textbf{Tristate value} & \textbf{Literal 1} & \textbf{Literal 2} \\ \hline
    n & 0 & 0 \\ \hline
    y & 1 & 0 \\ \hline
    m & 1 & 1 \\ \hline
  \end{tabular}
  \label{fig:tristate}
\end{table}

To aid in the constraints encoding, a set of additional literals are allocated
for each configuration option. Besides one literal for a \texttt{bool} or two
literals for a \texttt{tristate}, literals for five additional properties are
allocated. The additional literals encode if its value has been explicitly set
by the user, if it has been \texttt{select}ed by another configuration option,
if its prompt's dependencies have been satisfied, if its defaults' expressions
have been satisfied, and if the value must be set implicitly. The literals
allocated for each configuration option, and the number of instances of them,
are summarized in Table~\ref{fig:symbol}. The last literal, that forces the
option's value to be set implicitly, is more of a helper that is used when
simplifying a diagnosis by removing already indirectly set configuration option
values.

\begin{table}
  \centering
  \caption{Allocated literals for each configuration option.}
  \begin{tabular}{| l | p{5cm} | p{5cm} |}
    \hline
    \textbf{Literal} & \textbf{Used for} & \textbf{Allocated when} \\ \hline
    1 & The option's value (yes or no) & Always present \\ \hline
    2 & The option's value (module or not) & Present only if tristate \\ \hline
    3 & If the option's value has been set by the user & Always present \\
    \hline
    4 & If the option is \texttt{select}ed by another symbol & Always present
    \\ \hline
    5 & If the option's dependencies are satisfied & Only present if the option
    has a prompt attribute \\ \hline
    6 & If the option's default constraint is satisfied & One for each
    default value \\ \hline
    7 & Disallow the option's value to be set manually & Always present \\
    \hline
  \end{tabular}
  \label{fig:symbol}
\end{table}

With the Kconfig model declared in Listing~\ref{fig:example1}, the literals
listed in Table~\ref{fig:example1vars} are allocated. Two literals are
allocated for each \texttt{A} and \texttt{C} to hold their values, since they
are of the type tristate, while \texttt{B} only needs one. Each configuration
option gets the two mandatory literals that say if its value has been set by
the user and if it has been \texttt{select}ed by another configuration option.
All three configuration options have a prompt and are therefore allocated a
literal each that says if the configuration option's dependencies have been
satisfied. Lastly, since \texttt{B} has a default attribute, a literal is also
allocated that says if its \texttt{if} expression has been satisfied.

\begin{figure}
% config MODULES
%   bool "Enable loadable module support"
%   option modules
  \begin{lstlisting}[
    caption={A Kconfig model.},
    label={fig:example1},
  ]
config A
  tristate "A prompt"

config B
  bool "B prompt"
  select A
  default y

config C
  tristate "C prompt"
  depends on A && !B
  \end{lstlisting}
\end{figure}

\begin{table}
  \centering
  \caption{The literals allocated for the Kconfig model in
           Listing~\ref{fig:example1}.}
  \begin{tabular}{l l|l|l|}
    \cline{3-4}
    & & Literal & Used for \\
    \hline
    \multicolumn{1}{|c|}{\multirow{6}{*}{A}} & $A_1$ & 1 & Yes/no value \\
    \multicolumn{1}{|c|}{} & $A_2$ & 2 & Module/yes value \\
    \multicolumn{1}{|c|}{} & $A_3$ & 3 & If value is set by the user \\
    \multicolumn{1}{|c|}{} & $A_4$ & 4 & If the symbol has been selected \\
    \multicolumn{1}{|c|}{} & $A_5$ & 5 & If the prompt's dependency is fulfilled \\
    \multicolumn{1}{|c|}{} & $A_6$ & 6 & If the value must be implicitly set \\
    \hline
    \multicolumn{1}{|c|}{\multirow{6}{*}{B}} & $B_1$ & 7 & Yes/no value \\
    \multicolumn{1}{|c|}{} & $B_2$ & 8 & If value is set by the user \\
    \multicolumn{1}{|c|}{} & $B_3$ & 9 & If the symbol has been selected \\
    \multicolumn{1}{|c|}{} & $B_4$ & 10 & If the prompt's dependency is fulfilled \\
    \multicolumn{1}{|c|}{} & $B_5$ & 11 & If the default's dependency is fulfilled \\
    \multicolumn{1}{|c|}{} & $B_6$ & 12 & If the value must be implicitly set \\
    \hline
    \multicolumn{1}{|c|}{\multirow{6}{*}{C}} & $C_1$ & 13 & Yes/no value \\
    \multicolumn{1}{|c|}{} & $C_2$ & 14 & Module/yes value \\
    \multicolumn{1}{|c|}{} & $C_3$ & 15 & If value is set by the user \\
    \multicolumn{1}{|c|}{} & $C_4$ & 16 & If the symbol has been selected \\
    \multicolumn{1}{|c|}{} & $C_5$ & 17 & If the prompt's dependency is fulfilled \\
    \multicolumn{1}{|c|}{} & $C_6$ & 18 & If the value must be implicitly set \\
    \hline
  \end{tabular}
  \label{fig:example1vars}
\end{table}

\subsection{Constraints encoding}

Various propositional logic constraints can be inferred from a Kconfig model,
and their purpose is to limit the configurations to what the Kconfig language
permits. Since a SAT solver is being used, the constraints must be written as
CNF clauses, which are also easily expressed in the DIMACS format.

\subsubsection{Tristate}

A \texttt{tristate} has three states and is encoded with two literals, as
depicted in Table~\ref{fig:tristate}. The state $(0, 1)$ is invalid, and this
is enforced by the constraint:
$$(\mathit{Literal2} \to \mathit{Literal1}) \equiv (\lnot \mathit{Literal2}
\lor \mathit{Literal1})$$
If the second literal is true and the first literal is false, which happens in
the invalid state, the clause evaluates to false. Continuing with the Kconfig
example in Listing~\ref{fig:example1} and its literals allocation depicted in
Table~\ref{fig:example1vars}, this constraint gives rise to the following two
CNF clauses:
$$(\lnot A_2 \lor A_1) \equiv (\lnot 2 \lor 1)$$
$$(\lnot C_2 \lor C_1) \equiv (\lnot 14 \lor 13)$$
In DIMACS they correspond to \texttt{-2 1 0} and \texttt{-14 13 0}.

\subsubsection{Select}

If a configuration option is activated and it has a \texttt{select} attribute,
the configuration option it \texttt{select}s is also activated.

In the example in Listing~\ref{fig:example1}, \texttt{B} has the attribute
\texttt{select A}. This translates to the propositional formula $(B_1 \to A_4)
\equiv (\lnot B_1 \lor A_4) \equiv (\lnot 7 \lor 4)$, which means that if
\texttt{B} is set to \texttt{y}, \texttt{A} is \texttt{select}ed. In DIMACS it
is equal to \texttt{-7 4 0}.

Furthermore, to enforce that whenever a configuration option is selected a
lower bound is created, we need some more constraints. These are $(A_4 \to A_1)
\equiv (\lnot A_4 \lor A_1)$, $(B_3 \to B_1) \equiv (\lnot B_3 \lor B_1)$, and
$(C_4 \to C_1) \equiv (\lnot C_4 \lor C_1)$. In DIMACS, these constraint
clauses are equal to \texttt{-4 1 0}, \texttt{-9 7 0} and \texttt{-16 13 0}.
For instance, when \texttt{A} is \texttt{select}ed, which happens when $A_4$ is
true, it has to be set to either \texttt{m} or \texttt{y}. Note that this is a
simplification that does not deal with the complete complexity of
\texttt{select}'s lower bound.

\subsubsection{Prompt}

A configuration option's dependency, specified with the attribute
\texttt{depends on}, controls the visibility of the configuration option's
prompt. If the dependency is not satisfied, then the prompt is invisible and
the user is not able to change the configuration option's value.

All three configuration options in Listing~\ref{fig:example1} have a prompt.
For \texttt{A} and \texttt{B}, the prompt is implicitly always enabled since no
dependency has been declared. \texttt{A}'s literal that specifies if its
prompt's dependency is fulfilled is $5$. The CNF formula is therefore simply
$(5)$, and in DIMACS the clause is \texttt{5 0}. For \texttt{B}, the DIMACS
clause is equal to \texttt{10 0} in an analogous way.

\texttt{C} on the other hand has the dependency \texttt{A \&\& !B}, which
translates into the propositional formula $C_5 = A_1 \land \lnot B_1 \equiv 17
= (1 \land \lnot 7)$. In CNF, it is equal to the three clauses $(17 \lor \lnot
1 \lor 7)$, $(\lnot 17 \lor 1)$ and $(\lnot 17 \lor \lnot 7)$~\cite{tseytin}.
In DIMACS, the clauses are equal to \texttt{17 -1 7 0}, \texttt{-17 1 0} and
\texttt{-17 -7 0}. Note that this is a simplification that does not deal with
the complete complexity of \texttt{depends on}'s upper bound.

\subsubsection{Default}

A configuration option gets its \texttt{default} value if the \texttt{default}
attribute's optional constraint is fulfilled, the option is not
\texttt{select}ed by another option and the user has not already assigned a
value to it.

$A$ has the implicit default value \texttt{n}. It gets its default value if it
is not \texttt{select}ed by another symbol and the user has not given it a
value. This translates into the propositional formula $((\lnot A_3 \land \lnot
A_4) \to \lnot A_1) \equiv \{\text{De Morgan's law~\cite{de_morgan}}\} \equiv
(\lnot (A_3 \lor A_4) \to \lnot A_1) \equiv (A_3 \lor A_4 \lor \lnot A_1)
\equiv (3 \lor 4 \lor \lnot 1)$. In DIMACS it is equal to \texttt{3 4 -1 0}.

$B$ has the explicit default value \texttt{y}. If it has not been
\texttt{select}ed, the user has not assigned a value to it, and the
\texttt{default}'s constraint is fulfilled, it should fall back on its default
value \texttt{y}. This yields us the constraint $((\lnot B_2 \land \lnot B_3
\land B_5) \to B_1) \equiv (\lnot (B_2 \lor B_3 \land \lnot B_5) \to B_1)
\equiv (B_2 \lor B_3 \lor \lnot B_5 \land B_1) \equiv (8 \lor 9 \lor \lnot 11
\land 7)$, which in DIMACS is \texttt{8 9 -11 7 0}. Furthermore, since the
\texttt{default} attribute lacks an \texttt{if} expression, it is always true,
and we also get the additional constraint clause $(B_5) \equiv (11)$, which in
DIMACS is \texttt{11 0}.

$C$ also has the implicit default value \texttt{n}. The CNF clause is equal to
$(15 \lor 16 \lor \lnot 13)$ and the DIMACS formula is equal to \texttt{15 16
-13 0}.

\subsubsection{Must get its value from somewhere}

The default value of a symbol is \texttt{n}. If it has no explicit default, no
prompt and is not being \texttt{select}ed, it falls back on the value
\texttt{n}. Having any other value than \texttt{n} implies that it got its
value from a source.

Returning to the Kconfig example and looking at the configuration option $B$,
$10$ says if its prompt is visible, $9$ says if it is \texttt{select}ed, $11$
says if its default is satisfied, and $7$ is its value. This translates to $(7
\to (9 \lor 10 \lor 11)) \equiv (\lnot 7 \lor 9 \lor 10 \lor 11)$. In other
words, to be be able to set its value to \texttt{y}, one of its value sources
must first be true. This constraint is created in an analogous way for $A$ and
$C$.

\subsubsection{Force an implicit value}

This constraint does not originate from the Kconfig language. Its purpose is to
make the seventh literal in Table~\ref{fig:symbol} act as a toggle for the
configuration option's value source. If the literal is set to true, the
configuration option must get its value indirectly from either a default or
from being selected. If it is false, it has no effect on the satisfiability.

If we look at configuration option $B$ from the Kconfig example, its constraint
is $(B_6 \to (B_3 \lor B_5)) \equiv (12 \to (9 \lor 11)) \equiv (\lnot 12 \lor
9 \lor 11)$. Now, by setting literal $12$ to true, it enforces configuration
option $B$ to obtain a value from its \texttt{default} or from being
\texttt{select}ed. The satisfiability solver will return \texttt{unsat} if this
enforcement is not possible to make.

\section{Read and set the configuration}
\label{read_and_set_configuration}

The user's configuration is stored in the file \texttt{.config}. An excerpt of
how it could look like can be seen in Listing~\ref{fig:example3}. We can in the
example see that \texttt{A} is set to \texttt{y} and \texttt{C} is set to
\texttt{m}. What is not so obvious is that \texttt{B} is explicitly set to
\texttt{n}, even though it appears like the line about \texttt{CONFIG\_B} is
just a comment. However, it is only when a configuration option is completely
absent from the configuration file that it falls back on its default value,
specified in the \texttt{Kconfig} file.

\begin{figure}
  \begin{lstlisting}[
    caption={A \texttt{.config} excerpt.},
    label=fig:example3,
  ]
#
# Linux/x86 4.4.10 Kernel Configuration
#
CONFIG_A=y
# CONFIG_B is not set
CONFIG_C=m
  \end{lstlisting}
\end{figure}

We will now look at how the user's configuration is set in the SAT problem.
Assume that the configuration in Listing~\ref{fig:example3} is for the Kconfig
model declared in Listing~\ref{fig:example1}, with the allocated literals in
Table~\ref{fig:example1vars}. From this model and configuration, we get the
constraints $(1 \land \lnot 2)$, $(\lnot 7)$ and $(13 \land 14)$. These will
enforce the values of the three configuration options \texttt{A}, \texttt{B}
and \texttt{C}. Furthermore, we will also set the literals $3$, $8$ and $15$ to
true, which say that these configuration options got their values from the
user. Combined, they translate into the following single-variable clauses in
DIMACS:
\vspace{0.3cm}
\begin{verbatim}
  1 0
  -2 0
  3 0
  -7 0
  8 0
  13 0
  14 0
  15 0
\end{verbatim}

\section{Generate unsatisfiable cores with SAT}
\label{generate_unsatisfiable_cores_sat}

In this section, our method for extracting unsatisfiable cores in C with a SAT
solver is presented. With a small example containing both hard and soft
constraints, how the Scala implementation of RangeFix achieves this with an SMT
solver is first examined. Next, how we achieve the same thing with SAT in C is
presented. This highlights the contrast between the SMT and SAT syntax, and
their respective capabilities.

An example of two hard and three soft constraints is depicted in
Figure~\ref{fig:example2}. A satisfiability solver will evaluate this to
unsatisfiable, since $C \to \lnot B \equiv true \to \lnot true \equiv true \to
false \equiv false$. To regain satisfiability, some of the soft constraints
need to be edited.

\begin{figure}
  \begin{equation*}
    \begin{split}
      & [1]\ (A \to B) \\
      & [2]\ (C \to \lnot B) \\
      & [3]\ (A := true) \\
      & [4]\ (B := true) \\
      & [5]\ (C := true)
    \end{split}
  \end{equation*}
  \caption{Two hard constraints and three soft constraints.}
  \label{fig:example2}
\end{figure}

The two minimal unsatisfiable cores in this example are $\{2, 4, 5\}$ and $\{1,
2, 3, 5\}$. If any of the clauses that appears in a minimal unsatisfiable core
is removed, the rest of the core's clauses become satisfiable. For instance, if
clause $5$ is removed, then the core $\{2, 4, 5\}$ becomes satisfiable because
$C$ may be assigned to false.

\subsection{How unsatisfiable cores are generated with SMT}

The Scala implementation of the RangeFix algorithm depends on an SMT solver,
more specifically the SMT solver Z3. The SMT-LIB language has a command called
\texttt{get-unsat-core}, which responds with one unsatisfiable core if the
previous call to \texttt{check-sat} returned \texttt{unsat}. All solvers do not
support the \texttt{get-unsat-core} command~\cite{smtlib_v2}, however, Z3 is
one of those which supports it, making it suitable for the RangeFix algorithm.

The hard and soft constraints from Figure~\ref{fig:example2} are encoded with
the commands depicted in Listing~\ref{fig:example2smt}. The variables are
declared using constants, while the constraints are declared with assert
statements. Executing the SMT program prints:
\begin{verbatim}
  unsat
  (B C)
\end{verbatim}
If line number 13 is deleted, it will instead print the other core: \texttt{(A
C)}. An advantage, as illustrated in this example, is that with SMT it is
easily controlled what variables are returned as parts of the unsatisfiable
cores.

\begin{figure}
  \begin{lstlisting}[
    caption={Encoding the example in Figure~\ref{fig:example2} as an SMT
             problem.},
    label={fig:example2smt},
  ]
(set-option :produce-unsat-cores true)
(declare-const a Bool)
(declare-const b Bool)
(declare-const c Bool)
(declare-const _a Bool)
(declare-const _b Bool)
(declare-const _c Bool)
(assert (=> a b))
(assert (=> c (not b)))
(assert (or (not _a) (= a true)))
(assert (or (not _b) (= b true)))
(assert (or (not _c) (= c true)))
(assert (! _a :named A))
(assert (! _b :named B))
(assert (! _c :named C))
(check-sat)
(get-unsat-core)
  \end{lstlisting}
\end{figure}

\subsection{How to generate unsatisfiable cores with SAT}

{\sloppy

The propositional logical formulas in Figure~\ref{fig:example2} can also be
expressed as the CNF clauses in Listing~\ref{fig:example2sat}, encoded in the
DIMACS syntax, making them compatible with SAT solvers. An example of such a
SAT solver is PicoSAT~\cite{picosat}. By using logical
equivalence~\cite{logical_equivalence}, the \textit{implies} connectives have
been expressed with logical \textit{or} instead. With these clauses declared,
PicoSAT's function \texttt{picosat\_sat} returns
\texttt{PICOSAT\_UNSATISFIABLE}. Having evaluated the satisfiability, iterating
over the clauses and calling \texttt{picosat\_coreclause} during each
iteration, yields the responses \texttt{false}, \texttt{true}, \texttt{false},
\texttt{true} and \texttt{true}, i.e. the second, fourth and fifth clauses
belong to the unsatisfiable core, which translates to the core $\{2, 4, 5\}$.
If line number 5 is deleted, PicoSAT will instead return \texttt{true},
\texttt{true}, \texttt{true} and \texttt{true}, which translates to the core
$\{1, 2, 3, 5\}$. There clearly exists no distinction between hard and soft
constraints, and the unsatisfiable cores contain both constraint types.
However, since it is known in what order the clauses were added, it is trivial
to filter out the soft constraints from the unsatisfiable cores. By using this
technique, it is possible to get the diagnoses $\{B, C\}$ and $\{A, C\}$.

}

\begin{figure}
  \begin{lstlisting}[
    caption={Encoding the example in Figure~\ref{fig:example2} as a SAT
             problem.},
    label={fig:example2sat},
  ]
p cnf 3 5
-1 2 0
-2 -3 0
1 0
2 0
3 0
  \end{lstlisting}
\end{figure}

A code example where PicoSAT is utilized can be seen in
Listing~\ref{fig:example2satc}, which prints \texttt{B C} to the console. Where
the soft constraints begin is stored in the variable \texttt{config\_start},
which marks the spot of where to begin iterating over the clauses to identify
the ones that belong to the unsatisfiable core.

\begin{figure}
  \begin{lstlisting}[
    style=myc,
    caption={Extracting an unsatisfiable core in C with PicoSAT.},
    label=fig:example2satc,
  ]
#include <stdio.h>
#include "picosat.h"

#define number_of_configs 3

int main(int argc, char *argv[]) {
  PicoSAT *ps = picosat_init();
  picosat_enable_trace_generation(ps);

  int a = picosat_inc_max_var(ps);
  int b = picosat_inc_max_var(ps);
  int c = picosat_inc_max_var(ps);
  char *names[number_of_configs] = {"A", "B", "C"};

  picosat_add_arg(ps, -a, b, 0);
  picosat_add_arg(ps, -c, -b, 0);
  int config_start = picosat_add_arg(ps, a, 0);
  picosat_add_arg(ps, b, 0);
  picosat_add_arg(ps, c, 0);

  if (picosat_sat(ps, -1) == PICOSAT_UNSATISFIABLE) {
    for (int i = config_start;
         i < picosat_added_original_clauses(ps);
         ++i)
      if (picosat_coreclause(ps, i) == 1)
        printf("%s ", names[i - config_start]);
    printf("\n");
  }
  return 0;
}
  \end{lstlisting}
\end{figure}

\section{Generate diagnoses}
\label{generate_diagnoses}

In this section, how the diagnoses are generated in the C implementation of
RangeFix is examined. The algorithm works as explained in
Section~\ref{rangefix}, however, some additional implementation details will be
given here.

To start with, we will look at an example that illustrates how the diagnoses
are generated by the algorithm. Assume that we have the Kconfig model in
Listing~\ref{fig:kconfig4} and the configuration in Listing~\ref{fig:config4}.
The soft constraints are \texttt{\{A := no, B := no, D := yes\}}. During the
first iteration of RangeFix, the partial diagnosis is \texttt{\{\}} and the
configuration consists of \texttt{\{A, B, D\}}. The core it finds is
\texttt{\{B, D\}}, and the two partial diagnoses \texttt{\{A\}} and
\texttt{\{B\}} are added. During the second iteration, it randomly selects a
partial diagnosis to continue with. Assume that the partial diagnosis
\texttt{\{B\}} gets picked; the configuration consists then of \texttt{\{A,
D\}}. This returns the core \texttt{\{A, D\}}. The two partial diagnoses are
now \texttt{\{D\}} and \texttt{\{A, B\}}. Next, it continues with the partial
diagnosis \texttt{\{D\}}, which manages to satisfy the constraints and is added
to the list of generated diagnoses. The remaining partial diagnosis,
\texttt{\{A, B\}}, also manages to satisfy the constraints. The two produced
diagnoses are therefore \texttt{\{D\}} and \texttt{\{A, B\}}. This makes sense,
because we can either satisfy the constraints by setting \texttt{D} to
\texttt{n}, or by setting \texttt{A} and \texttt{B} to \texttt{y}.

\begin{figure}
  \begin{subfigure}[t]{0.5\textwidth}
    \begin{lstlisting}[
      caption={A Kconfig model.},
      label={fig:kconfig4},
    ]
config A
  bool "A"

config B
  bool "B"
  depends on A

config C
  bool
  default y if B

config D
  bool "D"
  depends on C
    \end{lstlisting}
  \end{subfigure}
  \begin{subfigure}[t]{0.5\textwidth}
    \begin{lstlisting}[
      caption={A configuration for the Kconfig model in
      Listing~\ref{fig:kconfig4}.},
      label={fig:config4},
    ]
CONFIG_A=n
CONFIG_B=n
CONFIG_D=y
    \end{lstlisting}
  \end{subfigure}
\end{figure}

How the user starts with a small configuration and expands it to contain an
additional configuration option is illustrated in Figure~\ref{diagnoses_imgs}.
We start with the set of all configuration options declared in the Kconfig
model, named $S$ in Figure~\ref{img1}. The user has a configuration in her
\texttt{.config} file, where she has configured a subset of all present
configuration options. This is depicted in Figure~\ref{img2}, where the
configuration $C$ is a subset of $S$. The configuration option the user wants
to enable is called $f$ and is depicted in Figure~\ref{img3}. $f$ is drawn
outside the area of $C$, because the user has not yet assigned a value to it in
her \texttt{.config} file. However, to enable $f$, its dependencies must also
be satisfied. This means that some of the configuration options that $f$
depends on must be edited too, illustrated by the green area in
Figure~\ref{img4}. Some of the dependencies have already been configured by the
user and have to be updated, which is depicted by the intersection of the green
and red areas. When $f$ and its dependencies have been properly configured, we
end up with a new configuration $C'$, shown in Figure~\ref{img5}. Note that the
new configuration $C'$ contains a larger set of configuration options,
including the configuration option $f$.

\begin{figure}
  \centering
  \begin{subfigure}[t]{0.30\textwidth}
    \resizebox{\columnwidth}{!}{\input{img1.pdf_tex}}
    \caption{The complete set of configuration options of the model $S$.}
    \label{img1}
  \end{subfigure}
  \hfill
  \begin{subfigure}[t]{0.30\textwidth}
    \resizebox{\columnwidth}{!}{\input{img2.pdf_tex}}
    \caption{The configuration $C$ from \texttt{.config} is a subset of the
    whole set of declared configuration options.}
    \label{img2}
  \end{subfigure}
  \hfill
  \begin{subfigure}[t]{0.30\textwidth}
    \resizebox{\columnwidth}{!}{\input{img3.pdf_tex}}
    \caption{We want to change the value of a configuration option $f$.}
    \label{img3}
  \end{subfigure}
  \begin{subfigure}[t]{0.30\textwidth}
    \resizebox{\columnwidth}{!}{\input{img4.pdf_tex}}
    \caption{A diagnosis which requires us to set new options' values and edit
    some of our previous configuration choices has been generated.}
    \label{img4}
  \end{subfigure}
  \hskip .5cm
  \begin{subfigure}[t]{0.30\textwidth}
    \resizebox{\columnwidth}{!}{\input{img5.pdf_tex}}
    \caption{Our new configuration $C'$ now contains the configuration option
    $f$.}
    \label{img5}
  \end{subfigure}
  \caption{Visualizing how a configuration option's value is edited, a
  diagnosis computed and the configuration expanded.}
  \label{diagnoses_imgs}
\end{figure}

For this to work, we cannot only have the configuration options in $C \cup
\{f\}$ as our soft constraints, because then the unsatisfiable cores will only
contain configuration options from this set. If the soft constraints do only
consist of configuration options from $C \cup \{f\}$, i.e. the union of the red
area and the blue dot in Figure~\ref{img3}, the satisfiability solver can then
freely infer values for the configuration options in $S \textbackslash (C \cup
\{f\})$. The configuration options that will be found among the unsatisfiable
cores are therefore only those in the green area in Figure~\ref{img4} that
overlap with $C$. To solve this, we include the whole set of configuration
options from the feature model as soft constraints, as illustrated by
Figure~\ref{img6}. The configuration options in $S \textbackslash (C \cup
\{f\})$ are set to their default values, which are computed by the internal
Kconfig infrastructure and read from the \texttt{rootmenu} data structure.

\begin{figure}
  \centering
  \resizebox{0.3\textwidth}{!}{\input{img6.pdf_tex}}
  \caption{Visualizing how all configuration options in $S$ are used as soft
  constraints.}
  \label{img6}
\end{figure}

We will now look at an example of how the soft constraints look like. Assume
that we have the Kconfig model in Listing~\ref{soft_constraints_kconfig} and
the configuration in Listing~\ref{soft_constraints_config}. There are three
configuration options, where one of them has been configured by the user in the
configuration file. When the configuration is read by the internal Kconfig
infrastructure, the values for all configuration options are computed. By
reading the \texttt{rootmenu} data structure, we find the three configuration
options' values. Even though only \texttt{A} has been explicitly assigned a
value by the user in the configuration file, the two others' values are also
easily retrieved by iterating over all configuration options using the Kconfig
infrastructure's function \texttt{for\_all\_symbols}. The configuration, and
the soft constraints, that the C implementation of RangeFix uses are then
\texttt{[A:=y, B:=y, C:=n]}.

\begin{figure}
  \begin{subfigure}[t]{0.5\textwidth}
    \begin{lstlisting}[
      caption={A Kconfig model.},
      label={soft_constraints_kconfig},
    ]
config A
  bool "A"

config B
  bool "B"
  default y

config C
  bool "C"
    \end{lstlisting}
  \end{subfigure}
  \begin{subfigure}[t]{0.5\textwidth}
    \begin{lstlisting}[
      caption={A configuration for the Kconfig model in
      Listing~\ref{soft_constraints_kconfig}.},
      label={soft_constraints_config},
    ]
CONFIG_A=y
    \end{lstlisting}
  \end{subfigure}
\end{figure}

\section{Simplify diagnoses}
\label{simplify_diagnoses}

In the previous section, it was explained that all configuration options are
used as soft constraints. It was also explained that the values used for these
configuration options in the soft constraints are parsed from the
configuration. This means that a diagnosis will contain all configuration
options whose values deviate from the values parsed from the configuration.
This includes configuration options that become implicitly \texttt{select}ed or
whose default value is changed.

{\sloppy

Let us take a look at an example of how this looks like in practice. Starting
with an \texttt{allnoconfig} for Linux kernel 4.4.10, assume that we generate
diagnoses for setting the configuration option
\texttt{IMA\_DEFAULT\_HASH\_SHA512} to \texttt{y}. A diagnosis for
\texttt{IMA\_DEFAULT\_HASH\_SHA512}, which contains all configuration options
whose values are changed, is \texttt{[INTEGRITY, CRYPTO\_SHA512, MULTIUSER,
CRYPTO, IMA, CRYPTO\_SHA1, CRYPTO\_MD5, SECURITYFS, CRYPTO\_HMAC,
CRYPTO\_MANAGER, TCG\_TPM, TCG\_TIS, SYSFS, SECURITY, IMA\_NG\_TEMPLATE]}.
However, the user does not need to manually change all these configuration
options by herself. The reason is that many of them are indirectly changed by
other configuration options through \texttt{select} and \texttt{default}
attributes. Looking at the definition of \texttt{IMA\_DEFAULT\_HASH\_SHA512} in
Listing~\ref{simplify_configs}, it is part of a \texttt{choice} group that
depends on \texttt{IMA}. However, \texttt{IMA} selects many other configuration
options, which therefore become part of the diagnosis. But, a better diagnosis
would only contain the smallest set of configuration options that the user is
required to edit. In this example, a simplified diagnosis is
\texttt{[CRYPTO\_SHA512, MULTIUSER, IMA, SYSFS, SECURITY]}. By only changing
the values of these five configuration options, it is possible to satisfy the
dependencies for \texttt{IMA\_DEFAULT\_HASH\_SHA512}. However, as we have seen,
other configuration options are also implicitly changed.

}

\begin{figure}
  \begin{lstlisting}[
    style=myc,
    caption={The definitions of the configuration options \texttt{IMA} and
    \texttt{IMA\_DEFAULT\_HASH\_SHA512}.},
    label=simplify_configs,
  ]
config IMA
  bool "Integrity Measurement Architecture(IMA)"
  select SECURITYFS
  select CRYPTO
  select CRYPTO_HMAC
  select CRYPTO_MD5
  select CRYPTO_SHA1
  select CRYPTO_HASH_INFO
  ...

choice
  prompt "Default integrity hash algorithm"
  depends on IMA
  ...

...

  config IMA_DEFAULT_HASH_SHA512
    bool "SHA512"
    depends on CRYPTO_SHA512 && !IMA_TEMPLATE
    ...

...

endchoice
  \end{lstlisting}
\end{figure}

To simplify a diagnosis, our C implementation of RangeFix uses the seventh
allocated configuration option literal, see Table~\ref{fig:symbol} for a list
of all allocated literals for each configuration option. When a diagnosis has
been found, the program iterates over each configuration option in the
diagnosis, sets the seventh literal to true and checks if the configuration is
still satisfiable. If it is satisfiable, it means that the configuration option
does not need to be explicitly changed by the user. But if it is not
satisfiable, it means that the user must manually change this configuration
option. Doing this process for each configuration option in a generated
diagnosis, it is possible to remove the configuration options that the user
does not need to change manually, which increases the usability of the
diagnoses.

\section{Integrate with xconfig}
\label{integrate_with_xconfig}

In Section~\ref{featuremodeling}, screenshots of the configurators
pure::variants and the eCos Configuration Tool were shown. Both of them support
the ability to enter an invalid state and later resolving any conflicts. To
help the user resolve conflicts, they detect and show conflicts in a panel
adjacent to the feature model. In the same section, it was also explained that
the Kconfig configurators do not support the configuration to enter an invalid
state. This is achieved by only presenting configuration alternatives that do
not cause any conflicts. For instance, assume that there are two configuration
options \texttt{A} and \texttt{B}, they are both tristates, and \texttt{A}
depends on \texttt{B}. If \texttt{B} is set to \texttt{m}, the configurator
does only let the user to set \texttt{A} to either \texttt{n} or \texttt{m}.
This is shown in Figure~\ref{xconfig_valid_states}, where "N" and "M" are the
configuration options' current values, and the underscores show the valid
states they may be changed to.

\begin{figure}
  \centering
  \includegraphics[width=0.4\textwidth]{images/xconfig_valid_states.png}
  \caption{Configuration option \texttt{A} depends on \texttt{B}, and is
  prohibited by the configurator to be set to \texttt{y}.}
  \label{xconfig_valid_states}
\end{figure}

The Kconfig configurators are built to only allow the configuration to enter
valid states, which has shaped the source code to a large extent. When building
a GUI prototype with xconfig, it was therefore easier to leave that code as-is,
and add the conflict-resolution as dependency-resolution instead. Rather than
assisting the user with resolving conflicts, it would assist the user by
generating fixes for satisfying unmet dependencies. This does not allow the
user to enter an invalid configuration state, but does still achieve the goal
of helping the user to enable disabled configuration options.

When this thesis project started, the existing Scala implementation of RangeFix
had not seen major updates for the last few years. This meant that the latest
version of the Linux kernel that it had been tested with was 2.6.32. LVAT,
which the Scala implementation of RangeFix depends on, had also not been
touched for many years. For instance, exconfig, which utilizes the internal
Kconfig infrastructure to produce an \texttt{.exconfig} file that is used as
input to RangeFix, did not work with version 4.4 of the Linux kernel, which was
the latest kernel version at this project's start. To demonstrate and evaluate
the concept of interactive dependency-resolution for Kconfig, implementing it
for the deprecated kernel version 2.6.32 had a lower barrier.

For evaluating the feasibility of implementing a SAT-based conflict-resolution
mechanism in C, there was no reason to use the deprecated 2.6.32 version of the
Linux kernel. The latest version of Satconfig, which our C implementation of
RangeFix utilizes, was based on version 4.4 of the Linux kernel. It was
therefore easiest, and it felt more relevant, to use a modern and supported
kernel version for evaluating the feasibility. The comparability might have
been slightly better if the C implementation of RangeFix had been implemented
for version 2.6.32 of the Linux kernel, as the Scala version. However, version
2.6.32 was first released in 2009 and has since then reached end-of-life.
