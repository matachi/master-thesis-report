\chapter{Demonstration}
\label{demonstration}

Two artifacts have been produced. They are both versions of xconfig, but with
different backends for the dependency-resolution support. The first uses the
Scala implementation of RangeFix as its backend, while the second uses the C
implementation of RangeFix as its backend.

\section{Configurator with Scala backend}
\label{configurator_with_scala_backend}

In Figure~\ref{xconfig_rangefix_scala1}, a screenshot of the modified version
of xconfig that calls the Scala implementation of RangeFix can be seen. With
its additional functionality, it is possible for the user to get assistance in
resolving configuration options' dependencies. In the screenshot, the
configuration option \texttt{TIFM\_CORE}, whose prompt is called "TI Flash
Media interface support", has been marked by the user. It is currently set to
no, indicated by the \texttt{N} in the column "N" to the right of it. Assume
that the user wants to change the value of the configuration option to yes, but
does not know how; by pressing the button in the toolbar that looks like a blue
C, a panel with functionality for calculating fixes will be opened.

\begin{figure}
  \centering
  \includegraphics[width=.8\textwidth]{images/xconfig_rangefix_scala1.png}
  \caption{A screenshot of the Linux configuration tool xconfig, enhanced with
  interactive dependency-resolution hidden behind a toolbar button.}
  \label{xconfig_rangefix_scala1}
\end{figure}

In Figure~\ref{xconfig_rangefix_scala2}, the panel for assisting the user with
calculating fixes has been opened. It consists of a toolbar, two lists and a
status bar. The buttons in the toolbar let the user perform various actions,
and will be explained in further detail in the following paragraphs. The upper
list is for containing configuration options that the user wants to configure,
while the lower list is for presenting generated fixes. Lastly, the status bar
provides the user with feedback, such as telling if RangeFix is running in the
background.

\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{images/xconfig_rangefix_scala2.png}
  \caption{The functionality to support interactive dependency-resolution has
  been opened in a separate panel of the window.}
  \label{xconfig_rangefix_scala2}
\end{figure}

With the buttons "N", "M" and "Y", the user is able to say what value she wants
for the marked configuration option in the left-hand panel. Assume that the
user wants to change the value of the configuration option \texttt{TIFM\_CORE}
from \texttt{N} to \texttt{Y}. It is currently not possible due to unmet
dependencies, indicated by the absence of an underscore in the column "Y" to
the right of the marked configuration option. But by pressing the button "Y" in
the toolbar to the right, the configuration option will be copied to the upper
list of the right panel. In Figure~\ref{xconfig_rangefix_scala3}, it is shown
how it looks like after the user has pressed the button "Y".
\texttt{TIFM\_CORE} has been copied to the list of configuration options that
the user wants to configure, its current value is \texttt{N}, and the value the
user wants it to have is \texttt{Y}.

\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{images/xconfig_rangefix_scala3.png}
  \caption{The user has added \texttt{TIFM\_CORE} to the list of configuration
  options she wants to configure.}
  \label{xconfig_rangefix_scala3}
\end{figure}

By selecting the configuration option in the upper-right list and pressing the
"Calculate fixes" button, RangeFix will be initiated. After a while, when the
commutation is completed, the fixes are returned and presented in the
lower-right list. In Figure~\ref{xconfig_rangefix_scala4}, fixes for the
configuration option \texttt{TIFM\_CORE} have been computed. Three fixes were
found in this case, two being visible in the screenshot. Any one of these fixes
is a valid way for resolving the dependencies and setting \texttt{TIFM\_CORE}
to \texttt{Y}.

\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{images/xconfig_rangefix_scala4.png}
  \caption{Fixes for the disabled configuration option \texttt{TIFM\_CORE} have
  been generated.}
  \label{xconfig_rangefix_scala4}
\end{figure}

This interface allows the user to first locate the configuration options she
wants to configure, and afterwards deal with their dependencies. An example
where the user has added three configuration options to the upper-right list is
shown in Figure~\ref{xconfig_rangefix_scala5}. The user has marked two of those
configuration options, and calculated fixes for them both at the same time. The
visible fix is much larger than the previous two fixes in
Figure~\ref{xconfig_rangefix_scala4}, but it will on the other hand result in
both \texttt{TIFM\_CORE} and \texttt{BT\_HCIBLUECARD} being set to yes if
applied.

The "Remove" button in the toolbar deletes marked configuration options from
the upper-right list. It is useful after a session where the user has been
wanting to edit some configuration options, computed fixes for them, applied
the fixes, and is now done with configuring them.

\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{images/xconfig_rangefix_scala5.png}
  \caption{Fixes for the both configuration options \texttt{TIFM\_CORE} and
  \texttt{BT\_HCIBLUECARD} have been generated.}
  \label{xconfig_rangefix_scala5}
\end{figure}

For a more thorough demonstration of the workflow that this configurator
enables, a video that showcases it has been recorded~\cite{video}.

\section{Configurator with C backend}
\label{configurator_with_c_backend}

In Figure~\ref{xconfig_rangefix_c1}, a screenshot of the modified version of
xconfig that uses the partial C implementation of RangeFix as its backend for
dependency-resolution can be seen. Overall, it works in the same way as the
other version. However, the style of the widgets look a bit different, because
this version of xconfig is compiled against Qt 4.8 or 5.x, while xconfig for
version 2.6.32 of the kernel is compiled against Qt 3.

By clicking the button in the toolbar that resembles the letter C, the
dependency-resolution panel in the right side of the window is opened. By
selecting a configuration option and clicking one of the "N", "M" and "Y"
buttons, the configuration option is added to the list of configuration options
in the middle-right part of the window. These are the configuration options
that the user wants to configure. Selecting one of the saved configuration
options in that list and clicking the "Calculate fixes" button, triggers the C
implementation of RangeFix. When RangeFix returns, the generated diagnoses are
listed in the bottom-right corner of the window.

\begin{figure}
  \centering
  \makebox[\textwidth][c]{
    \includegraphics[width=1.2\textwidth]{images/xconfig_rangefix_c1.png}}
  \caption{A screenshot of the Linux 4.4.10 configuration tool xconfig,
  enhanced with interactive conflict-resolution.}
  \label{xconfig_rangefix_c1}
\end{figure}
