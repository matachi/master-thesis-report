\chapter{Introduction}

Through software configuration, the users of a piece of software are able to
customize it to their needs. There are software systems with a wide range of
users, exposing an extensive set of configuration options to support a variety
of hardware and software needs. These options might have complex dependencies
between one another. Having an effective interactive configuration process that
aids the user in resolving violations of one or more of these dependencies is
therefore important.

For instance, the Linux kernel is one of the largest highly configurable
open-source software systems in existence today~\cite{bhartiya2016,
berger2013study}. A screenshot of its configuration tool xconfig can be seen in
Figure~\ref{screenshot}. It has more than $13{,}000$ configuration
options~\cite{dintzner2015analysing}, also called features. Other examples of
highly configurable systems are BusyBox, CoreBoot, eCos, and
uClibc~\cite{berger2013study}. Highly configurable systems appear in software
industries such as automotive, avionics, and communications equipment.

xconfig is not the only configurator that the Linux kernel comes bundled with.
The Linux kernel also includes other configurators called config, menuconfig and
gconfig that expose configuration of the same underlying feature
model~\cite{hintermann2007operating}. While config is a very basic prompt-based
interface, the other configurators offer a menu-based interface, such as the
one seen in Figure~\ref{screenshot}, using different front-end
toolkits~\cite{hintermann2007operating}.

\begin{figure}[!h]
  \centering
  \includegraphics[width=0.8\textwidth]{images/screenshot.png}
  \caption{A screenshot of the Linux configuration tool xconfig.}
  \label{screenshot}
\end{figure}

\section{Problem identification and motivation}
\label{motivation}

Building feature models with Kconfig, which is the feature modeling language
that the Linux kernel uses, occurs to a great extent. There are many people in
the world who either interact with the language directly as developers, or
indirectly using one of the many configurators. The Linux kernel has received
contributions from more than $2{,}000$ developers during a 15 months
period~\cite{brodkin2015}. Furthermore, between 2005 and 2015, nearly $1{,}000$
companies contributed to the Linux kernel~\cite{brodkin2015}. Taking into
account the number of people using the Linux kernel, developing it, and the
number of companies involved, it is the largest software development project in
history~\cite{bhartiya2016}. Besides the Linux kernel, there are also several
other open-source projects that use Kconfig for their feature model, including
axTLS, BuildRoot, BusyBox, CoreBoot, EmbToolkit, Fiasco, Fraeetz, ToyBox,
uClibc, uClinux-base and uClinux-dist~\cite{berger2013study}. Linux users
configure the kernel for reasons such as personal use, server maintenance,
system administration, development, embedded systems and virtual
machines~\cite{hubaux2012user}. Since Kconfig is open-source, it is impossible
to exactly quantify to what extent it is being used, but as noted here, there
clearly are a lot of developers, administrators, projects and companies who
interact with Kconfig in one or another way.

The task to configure the Linux kernel becomes more challenging as its feature
model continues to grow. With each new release of the Linux kernel, the code
base grows both in terms of source lines of code and configuration
options~\cite{koren2006study, passos2015feature}. Between 2005 and 2015, the
number of source lines of code in the kernel grew with $159\,\%$, or on average
with $2.6\,\%$ per release~\cite{passos2015feature}. Today, there are more than
$13{,}000$ configuration options in the Linux
kernel~\cite{dintzner2015analysing}, making it one of the largest known feature
models~\cite{berger2013study}. The number of Kconfig configuration options
grows linearly with the number of lines of code, due to the development of the
Linux kernel being predominantly feature-driven~\cite{lotufo2010evolution}.

Hubaux et al.~\cite{hubaux2012user} have conducted a survey to identify
challenges Linux and eCos users face when configuring their systems. 97 Linux
users filled out the survey, roughly half of whom claimed to be experts with up
to 20 years of experience with Linux. Half of the Linux user participants
reported that they make between 20 and 50 changes to a default configuration,
while some participants answered that they make more than $2{,}000$ changes.
$56\,\%$ of the participants consider enabling/disabling a configuration option
to be a problem in practice. $20\,\%$ of the Linux user participants stated
that they need at least a few dozen minutes on average to figure out how to
activate an inactive option. To activate an inactive option, $46\,\%$ of the
participants said that they manually read and follow the constraints in the
configuration option's help text. However, $19\, \%$ of the Linux user
participants complained that the help text documentation often is incomplete,
hard to understand or incorrect. $26\,\%$ participants said that they rely on
their own expertise to activate an inactive option. In the paper by Hubaux et
al., we find the following quote from one of the participants who provided a
more detailed response to the survey:
\vspace{0.5cm}
\begin{displayquote}
  "As far as consistency checking and helping determine inter-related
  dependencies on settings, I have long wished for a better kernel
  configuration tool [\,\ldots], but it seems that the kernel guys learn their
  way around the configurator by much exposure, and the rest of us have to just
  figure it out [\,\ldots]"
\end{displayquote}
\vspace{0.5cm}

Hubaux et al.~\cite{hubaux2012user} are not the only ones having identified the
configuration process of the Linux kernel as lacking; there are also kernel
developers who want to see improvements in this area. They have created a
project called kconfig-sat with a wiki page~\cite{kernelprojects_kconfig_sat},
where information about the initiative is organized, and a mailing
list~\cite{kconfig_sat_list}, where discussions take place. Their plan is to
integrate a boolean satisfiability solver, SAT solver for short, with the
kernel configurators to assist the users in resolving dependencies. As a side
note, they have also identified other areas in the kernel where a SAT solver
could be beneficial, such as process scheduling and dependency resolution
during boot up~\cite{kernelprojects_linux_sat}.

To illustrate the challenge in configuring the Linux kernel, let us look at a
practical example. For instance, the user may want to enable the configuration
option \texttt{RTLWIFI\_DEBUG}, which provides debugging information to Realtek
network adapters. But \texttt{RTLWIFI\_DEBUG} depends on the configuration
option \texttt{RTLWIFI}, which means that \texttt{RTLWIFI} has to be enabled
before \texttt{RTLWIFI\_DEBUG} can be enabled. However, \texttt{RTLWIFI} is
invisible and does not appear in the configurator. To enable it, a Realtek
network adapter must instead be enabled, for instance \texttt{RTL8192DE} or
\texttt{RTL8821AE}. These latter two options are utilizing a reverse-dependency
that enables \texttt{RTLWIFI} whenever they are enabled. To figure out why
\texttt{RTLWIFI\_DEBUG} is inactive and cannot be enabled, and to satisfy its
constraints, is not straightforward with the current configurators. Resolving
dependencies is often a time-consuming task where the user has to rely on his
own expertise or manually follow the constraints described in the
documentation~\cite{hubaux2012user}.

To summarize, Kconfig is a feature modeling language that a large user base
depends on in one way or another. The Linux kernel is interesting as a case
study target due to its large feature model and popularity. Lastly, both users
and developers of the Linux kernel have identified that a dependency-resolution
tool would be beneficial.

\section{Objectives}

The objective of this Master's thesis is to conduct a case study where the
feasibility of realizing an interactive conflict-resolution approach in one of
the Linux kernel configurators is investigated. An approach would be attempted,
and the effectiveness, scalability and practicability evaluated. By also
collecting feedback from the Linux community, the usability of such a solution
could be estimated. The results can be used to realize configuration processes
for similar domains, or assess the feasibility of it. To achieve this
objective, we address the following sub-goals:

\vspace{0.3cm}
\begin{itemize}

  \item Generate a logical representation of the Linux kernel's feature model
    usable by reasoners.

  \item Find an approach to detect conflicts.

  \item Implement an approach to generate fixes, which will ultimately resolve
    the conflicts.

  \item Create a prototype that integrates the above into the Linux kernel
    configurator xconfig.

\end{itemize}

\section{Research questions}

The main research question to be addressed is:

\vspace{0.3cm}
\begin{itemize}

  \item \textit{How to realize scalable conflict-resolution for a configuration
    system as large as the Linux kernel configurator?} The feasibility of
    implementing a conflict-resolution algorithm from academia, which meets the
    requirements of the community, will be investigated.

\end{itemize}
\vspace{0.3cm}

\noindent The sub-question is:

\vspace{0.3cm}
\begin{itemize}

  \item \textit{How to encode the problem?} The constraints of the Linux
    kernel's feature model must be encoded in such a way that it is possible to
    do reasoning with them. The choice of constraint solver depends on what the
    Linux kernel community prefers.

\end{itemize}

\section{Research design}

Our goal is to realize interactive conflict-resolution support for one of the
Linux kernel's configurators. This was initiated by a pre-study where available
tools and options were investigated, with the goal of finding an appropriate
algorithm for resolving unmet dependency. It was followed by the
construction of a prototype where the usefulness of interactive
conflict-resolution could be demonstrated. The feasibility of implementing the
algorithm in C with a boolean satisfiability solver, which has a stronger
community support, was also investigated. The last part of the thesis project
was to evaluate the accuracy of the fixes, the scalability of the algorithm, and
the usability of the prototype's user interface.

The research strategy for this project was a case study. In an exploratory
manner, the objective to implement and evaluate interactive conflict-resolution
in a highly configurable software has been researched. The context was software
configuration, while the case was narrowed down to the Linux kernel, and the
unit of analysis was this very project's produced artifacts. A frame of
reference and data collection were achieved through a survey with kernel
developers, where they provided their insights and feedback on the
implementation. The case was intentionally selected within the context with the
expectation of it to be revelatory due to its size and complexity.

The research method for this project was design science research, which can be
divided into six activities~\cite{peffers2006design}. The methodology is
explained in further detail in Chapter~\ref{methodology}, but a short overview
is given here together with an outline of this thesis. The first activity of
design science research is \textit{problem identification and motivation},
where the specific research problem is defined and the value of a solution
justified. This is elaborated in detail in Section~\ref{motivation}. The second
activity is \textit{objectives of a solution}, where objectives of the project
are inferred from the problem definition, which is done in detail in
Chapter~\ref{methodology}. The third activity is \textit{design and
development}, where artifacts are produced with the aim of addressing the
research question. The thought process behind the development and the produced
artifacts are presented in Chapter~\ref{design_and_development}. The fourth
activity is \textit{demonstration}, where the efficacy of the artifacts is
showcased. This is done in Chapter~\ref{demonstration}, where the
implementations are shown, and the workflow they support explained. The fifth
activity is \textit{evaluation}, where the artifacts are observed and measured
to determine how well they solve the identified problem. This was done through
correctness and performance analysis, as well as through a survey with kernel
users and developers, and the results are presented in
Chapter~\ref{evaluation}. The sixth activity is \textit{communication}, where
the problem and the artifacts are communicated, which is done through this
report. The results have also been shared with the Linux developers.
Furthermore, observed challenges that this thesis' C-based artifact has not yet
been able to solve or implement are elaborated in Chapter~\ref{towards_c}. This
provides information for the design and development activity for subsequent
projects aimed at improving the effectiveness of that artifact.
