\chapter{Background}

In this chapter, topics central for this thesis are explained. The first
section shows how feature modeling looks like in some software systems. Next,
an overview of the Kconfig feature modeling language is given. This is followed
by the presentation of two types of constraint solvers. Next, an overview of
the available Kconfig tools that were investigated for this thesis are
presented. One such tool, RangeFix, is explained in further detail in its own
section. Lastly, some related works are presented.

\section{Feature modeling}
\label{featuremodeling}

A feature model is a graphical representation of commonalities and differences
in a product line~\cite{hubaux2012feature}. The feature model consists of a
hierarchy of features~\cite{hubaux2012feature}, where a feature is an increment
in product functionality~\cite{batory2006automated}. Features are used to
capture functionalities and distinguish products in a product
line~\cite{batory2006automated}. Furthermore, a feature model does also define
constraints over the model to limit the number of valid
combinations~\cite{hubaux2012feature}. Each such valid feature combination is
called a product, or an instance of the feature model~\cite{hubaux2012feature}.

Examples of feature modeling software are pure::variants from pure-systems,
Gears from BigLever, and the eCos Configuration Tool. pure::variants is a
commercial tool where the user can build a feature model consisting of features
and restrictions between arbitrary features~\cite{purevariants}. These
restrictions are expressed with logical rules~\cite{purevariants}.
pure::variants has integrated conflict-resolution support, which is implemented
through a conversion of the constraints into Prolog where the dependencies are
evaluated~\cite{purevariants}. A screenshot of pure::variants can be seen in
Figure~\ref{purevariants1}. The window in the screenshot contains a feature
model in the left panel, relations in the right panel, and conflicts between
the configuration options in the bottom panel. In the bottom panel we can see
that there is a conflict between two of the configuration options. In
Figure~\ref{purevariants2}, a different view of pure::variants' feature model
can be seen. It displays the relations and conflicts more visually. Gears is
another commercial tool. It allows its user to build and manage a complete
product line through a feature model~\cite{gears}. eCos on the other hand is an
open source real-time operating system which is configured through a
configurator called eCos Configuration Tool~\cite{ecos}. The feature model is
specified with a language called Component Definition Language
(CDL)~\cite{ecos}. The eCos Configuration Tool also supports
dependency-checking and reports on any detected conflicts, which makes it
possible for its users to resolve the conflicts and ensure a consistent
configuration~\cite{ecos}. A screenshot of the eCos Configuration Tool can be
seen in Figure~\ref{ecos}. The configuration in the screenshot is in an
unsatisfied state and in the upper-right corner of the window we can see that
one conflict has been detected.

\begin{figure}
  \centering
  \includegraphics[width=0.8\textwidth]{images/purevariants1.png}
  \caption{A screenshot of the pure::variants configurator.}
  \label{purevariants1}
\end{figure}

\begin{figure}
  \centering
  \includegraphics[width=0.85\textwidth]{images/purevariants2.png}
  \caption{A different view of the feature model in pure::variants.}
  \label{purevariants2}
\end{figure}

\begin{figure}
  \centering
  \includegraphics[width=0.85\textwidth]{images/ecos.png}
  \caption{A screenshot of the eCos Configuration Tool.}
  \label{ecos}
\end{figure}

The Linux kernel is bundled with several configurators. These configurators can
be seen as feature modeling tools~\cite{sincero2008linux}. All of them expose
the same feature model, but with different front-ends. The most basic one is
called config, a screenshot of it can be seen in Figure~\ref{config}, which
prompts the user with questions to configure the kernel. Another configurator
is menuconfig, which can be seen in Figure~\ref{menuconfig}. It has a
terminal-based interface with menus that the user can navigate through and tick
configuration options on and off to configure the kernel. One of the
configurators with a graphical user interface is called xconfig, which can be
seen in Figure~\ref{screenshot}. Common among all Linux configurators is that
entering an invalid state is impossible due to only the valid configuration
option values being selectable~\cite{hubaux2012user}. This means that no
interactive conflict-resolution support in the configurators has been
implemented, as opposed to eCos and pure::variants. However, there is also no
built-in tool for helping the user to activate an inactive configuration option
through dependency-resolution.

\begin{figure}
  \centering
  \includegraphics[width=0.8\textwidth]{images/config.png}
  \caption{A screenshot of the Linux kernel configurator "config".}
  \label{config}
\end{figure}

\begin{figure}
  \centering
  \includegraphics[width=0.8\textwidth]{images/menuconfig.png}
  \caption{A screenshot of the Linux kernel configurator menuconfig.}
  \label{menuconfig}
\end{figure}

\section{Kconfig}
\label{kconfig}

The Linux kernel's feature model is specified with a language called Kconfig,
which at a high level is a collection of configuration options that are
organized in a tree structure~\cite{kconfig_language}. These configuration
options are sometimes also called symbols, menu entries, or features. An
example of a Kconfig file can be seen in Listing~\ref{fig:kconfig_example}. The
most relevant Kconfig concepts for this thesis, among the ones affecting
Kconfig's variability management logic~\cite{el2015analysing}, are config
options, attributes, choices, hierarchies, and propositional logic constraint
expressions.

\begin{figure}
  \begin{lstlisting}[
    caption={A partial Kconfig model with six configuration options.},
    label={fig:kconfig_example},
  ]
config MODVERSIONS
  bool "Set version information on all module symbols"
  depends on MODULES

config BT_MRVL_SDIO
  tristate "Marvell BT-over-SDIO driver"
  depends on BT_MRVL && MMC
  select FW_LOADER
  select WANT_DEV_COREDUMP

config NR_CPUS
  int "Maximum number of CPUs (2-32)"
  range 2 32
  depends on (AGP || AGP=n) && !EMULATED_CMPXCHG && MMU && HAS_DMA
  default "32" if ALPHA_GENERIC || ALPHA_MARVEL
  default "4" if !ALPHA_GENERIC && !ALPHA_MARVEL
  select I2C_ALGOBIT
  select DMA_SHARED_BUFFER
  help
    MARVEL support can handle a maximum of 32
    CPUs, all the others with working support
    have a maximum of 4 CPUs.

config A
  tristate "A prompt"
  depends on B

config B
  tristate "B prompt"

config C
  tristate "C prompt"
  select A
  \end{lstlisting}
  \begin{lstlisting}[
    caption={A Kconfig \texttt{.config} configuration file.},
    label={fig:config_example},
  ]
CONFIG_MODVERSIONS=y
CONFIG_BT_MRVL_SDIO=m
CONFIG_NR_CPUS=8
# CONFIG_A is not set
CONFIG_B=m
CONFIG_C=y
  \end{lstlisting}
\end{figure}

Config options are what the user modifies to configure the kernel. A
configuration option is specified in Kconfig with the keyword \texttt{config}.
Each configuration option has one of the five types \texttt{bool},
\texttt{tristate}, \texttt{string}, \texttt{hex} and \texttt{int}. Out of
these, \texttt{tristate} needs some further explanation. A \texttt{tristate}
behaves like a \texttt{bool}, except that it has three states: no, module, and
yes. By using \texttt{tristate}, it is possible for the user to specify whether
a configuration option, for instance a driver, should be compiled into the
kernel or be compiled as a loadable module. $94\,\%$ of the configuration
options for the x86 architecture are either of the type \texttt{bool} or
\texttt{tristate}~\cite{berger2013study}.

Attributes are used to enhance configuration options with various properties.
They are \texttt{prompt}, \texttt{default}, \texttt{range}, \texttt{visible
if}, \texttt{depends on}, and \texttt{select}. \texttt{prompt} sets the name of
the configuration option that appears in the configurator; if it is absent, the
config is invisible. \texttt{default} sets the configuration option's default
value. \texttt{range} is valid for the numerical types and specifies the range
that the configuration option's value can be set within. \texttt{visible if}
controls the visibility of the configuration option in the configurator. With
\texttt{depends on}, it is possible to specify an expression that must evaluate
to true for the user to be able to edit its value in the configurator.
\texttt{select} is a reverse-dependency; if a configuration option is enabled
and it is set to \texttt{select} a second configuration option, that second
configuration option is forced to also be enabled.

Choice is a group of \texttt{bool} or \texttt{tristate} configuration options
where only a single one may be set to yes, i.e. they are mutually exclusive. In
the case when the type is \texttt{tristate}, any number of configuration
options are also allowed to be set to module. A choice group is useful if there
exists multiple drivers for a single hardware but only one can be compiled into
the kernel while any number can be compiled as loadable
modules~\cite{kconfig_language}.

Hierarchies are built by dependencies and menus. Through dependencies, a
hierarchy can sometimes be inferred, where a configuration option becomes a
child to the configuration option it depends on. A menu, specified with the
keyword \texttt{menuconfig}, is a regular \texttt{config} except that it also
has sub-options which gives a hint to the configurator how the configuration
options should be presented. In configurators such as menuconfig and xconfig, a
\texttt{menuconfig} results in a new level in the tree view.

Constraint logic expressions can be used together with most attributes. The
supported operators for boolean and tristate logic are:
\begin{itemize}

  \item \textit{Constants}. There are three constants: no, \texttt{n} (=0);
    module, \texttt{m} (=1); and yes, \texttt{y} (=2).

  \item \textit{Negation}. Negation is achieved with \texttt{!<expression>},
    which returns the result of \texttt{2 - <expression>}. \texttt{y} turns
    into \texttt{n} and vice-versa, while \texttt{m} is unaffected.

  \item \textit{Equality}. The equality between two symbols, i.e. either a
    configuration option or a constant, is returned with \texttt{<symbol
    1>=<symbol 2>}. For example, \texttt{A=m} returns \texttt{y} if the
    configuration option \texttt{A} is set to \texttt{m}, otherwise \texttt{n}.

  \item \textit{Inequality}. This works like equality, but the other way
    around. It is specified with \texttt{<symbol 1>!=<symbol 2>} and returns
    \texttt{n} when the symbols are equal and \texttt{y} otherwise.

  \item \textit{Max}. The max-operator returns the largest value of two
    expressions, and the operator is written \texttt{<expression 1> ||
    <expression 2>}. For instance, if the expression is \texttt{n || m}, it
    will be evaluated to \texttt{m}.

  \item \textit{Min}. The min-operator returns the smallest value of two
    expressions, and the operator is written \texttt{<expression 1> \&\&
    <expression 2>}. For instance, if the expression is \texttt{y \&\& m}, it
    will be evaluated to \texttt{m}.

\end{itemize}
Using these operators, it is possible to write complex expressions for
controlling dependencies and other attributes. For instance, \texttt{depends on
(A \&\& B) || C} and \texttt{select A if B=m || C!=n}. More examples of
constraint logic formulas can be seen in Listing~\ref{fig:kconfig_example}.

There is also an attribute called \texttt{help} that does not affect Kconfig's
variability management~\cite{el2015analysing}. It is used for providing
documentation about the configuration options, which can be displayed in the
configurators for the users. A screenshot of how it looks like in xconfig for
the configuration option \texttt{OMAP2\_DSS} can be seen in
Figure~\ref{xconfig_help_text}. The description of the configuration option is
just "OMAP2+ Display Subsystem support." However, the help text in the
configurator also contains other useful information.

\begin{figure}
  \centering
  \includegraphics[width=.9\textwidth]{images/xconfig_help_text.png}
  \caption{Help text in xconfig for the configuration option
  \texttt{OMAP2\_DSS}.}
  \label{xconfig_help_text}
\end{figure}

Dependencies and reverse-dependencies give rise to upper and lower bounds. Let
us take a look at the three configuration options \texttt{A}, \texttt{B}, and
\texttt{C} in Listing~\ref{fig:kconfig_example}. If \texttt{B} is set to
\texttt{n}, the value of \texttt{A} cannot be changed by the user from
\texttt{n}. But if \texttt{B} is to \texttt{y} by the user, then \texttt{A} can
be set to any tristate value. However, if \texttt{B} is set to \texttt{m}, then
\texttt{A} can only be set to either \texttt{n} or \texttt{m}. This illustrates
how \texttt{depends on} creates an upper bound. In a similar manner,
\texttt{select} creates a lower bound. If \texttt{C} is set to \texttt{y}, then
\texttt{A} is also forced to \texttt{y} without the user being able to change
its value. If \texttt{C} is set to \texttt{m}, the user is free to set
\texttt{A} to either \texttt{m} or \texttt{y} (given that \texttt{B} is set to
\texttt{y}). But if \texttt{C} is set to \texttt{n}, the lower bound is
\texttt{n}.

The upper and lower bounds can also be combined. For instance, if both
\texttt{B} and \texttt{C} are set to \texttt{m}, then \texttt{A} is also set to
\texttt{m}; the reason being that \texttt{B} creates the lower bound
\texttt{m}, and \texttt{C} creates the upper bound \texttt{m}.

The configuration is stored in a file called \texttt{.config}. An example of
such a file can be seen in Listing~\ref{fig:config_example}. A hashtag
(\texttt{\#}) creates a comment. Any configuration option that has not been
assigned a value is implicitly set to \texttt{n}, if it does not have a
\texttt{default} attribute that overrides it to something else.

\subsection{Internal Kconfig infrastructure}
\label{internal_kconfig_infrastructure}

The kernel source tree includes several configurators and they do all share the
same code for reading and parsing Kconfig models and \texttt{.config}
configuration files. The function with the signature \texttt{void
conf\_parse(const char *name)} takes the path to a Kconfig file as its
parameter and loads the Kconfig model into a global variable with the signature
\texttt{struct menu rootmenu}. The configuration is loaded with the function
\texttt{int conf\_read(const char *name)}, which takes the path to a
\texttt{.config} file as its parameter. By loading a configuration, the
\texttt{rootmenu} data structure is updated with value assignments from the
configuration.

Some of the most important Kconfig C structs and their variables are shown in
Figure~\ref{datatypes}. Each list item in the configurator maps to an instance
of the struct \texttt{menu}. The list items are organized in a tree structure,
where one's children are pointed at by \texttt{menu}'s variable \texttt{list}.
A \texttt{menu} can also, with its \texttt{sym} variable, point at a
\texttt{symbol} struct, where the configuration option's data is found. A
\texttt{symbol}'s current configuration value is found in the variable
\texttt{curr}. The configuration option's attributes are pointed at by
\texttt{prop}, which are ordered as a single-linked list. The different
configuration option types are found in the enum \texttt{symbol\_type}, and the
configuration option attribute types are in the enum \texttt{prop\_type}.

\begin{figure}
  \centering
  \includegraphics[width=0.8\textwidth]{datatypes.eps}
  \caption{Internal Kconfig data structures.}
  \label{datatypes}
\end{figure}

\section{Constraint solvers}

There are several different types of solvers for working with constraint
satisfaction problems. Two popular choices are SAT and SMT, which are explained
in further detail below. The concept of unsatisfiable cores will also be
explained.

SAT, an abbreviation of \textit{boolean satisfiability problem}, is used for
modeling boolean constraint problems. It only supports boolean typed variables,
and the constraint formulas must often be written in CNF~\cite{cnf}
(conjunctive normal form). A common format for these problems is
DIMACS~\cite{challenge1993satisfiability}. An example of a DIMACS encoded
problem can be seen in Listing~\ref{fig:dimacs_example}; it consists of three
variables ($1$, $2$ and $3$) and the two clauses $(1 \lor \lnot 3) \land (2
\lor 3 \lnot \lor 1)$. The problem is satisfiable if there is an assignment of
$1$, $2$ and $3$ that makes all clauses evaluate to true. By running it through
a SAT solver, we can find out if the problem is satisfiable and what a valid
assignment of the variables would be in that case. The primary advantage with
SAT is its very good performance which enables it to do extremely fast
reasoning with thousands of variables~\cite{hubaux2012feature}.

\begin{figure}
  \begin{lstlisting}[
    caption={An example of a SAT problem written in DIMACS.},
    label={fig:dimacs_example},
  ]
p cnf 3 2
1 -3 0
2 3 -1 0
  \end{lstlisting}
  \begin{lstlisting}[
    caption={An example of an SMT problem written in SMT-LIB.},
    label={fig:smt_lib_example},
  ]
(declare-const a Int)
(declare-fun f (Int Bool) Int)
(assert (> a 10))
(assert (< (f a true) 100))
(check-sat)
  \end{lstlisting}
\end{figure}

SMT stands for \textit{satisfiability modulo theories} and is a generalization
of SAT. It provides a richer modeling language, where it is possible to work
with integers, arithmetic, functions and more. An example of an SMT problem,
written in the SMT-LIB language syntax, can be seen in
Listing~\ref{fig:smt_lib_example}. In the example, a constant \texttt{a} is
declared with the type \texttt{Int}, and a function \texttt{f}, taking two
arguments and returning one value, is also declared. Two asserts are made over
these declarations, and in the end the satisfiability of the problem is checked.
As we can see in this small example, with SMT many problems can be modeled more
easily than in SAT.

A concept related to constraint solvers is the notion of
\textit{unsatisfiable cores}, which is a subset of constraints in a
configuration that in standalone are unsatisfiable. Assume we have the boolean
variables $A$, $B$ and $C$, and the following constraints: $$A := true,\ B :=
false,\ C := true,\ A \leftrightarrow B,\ B \leftrightarrow C$$ The
configuration is unsatisfied since some of the constraints are violated. One
unsatisfiable core in this example is $\{A := true,\ B := false,\ A
\leftrightarrow B\}$, which means that if we only consider the constraints in
the core, the configuration is still unsatisfiable. The core is also minimal
because if we remove any of the constraints from the core, it is possible to
find an assignment of the variables that satisfies the constraints. For
instance, if we remove $B := false$, we only have the constraints $\{A :=
true,\ A \leftrightarrow B\}$ left, and we are able to find a valid assignment
to $A$, $B$ and $C$, such as $A := true,\ B := true,\ C := false$.

\section{Overview of available Kconfig tools}
\label{overview_kconfig_tools}

There are several tools and research projects available that can analyze
Kconfig models in various ways. Part of the project has been to investigate
these configuration options and see what could be re-used for this thesis
project. These are listed below.

\textit{Kconfigreader}~\cite{kconfigreader} is a tool implemented in Scala that
reads Kconfig files and converts them into CNF formulas in the DIMACS format
for further reasoning. It relies on a utility called \textit{dumpconf}, which
is implemented in C, that utilizes Linux's Kconfig infrastructure to dump the
internal representation as XML, which is then parsed by Kconfigreader.

\textit{Undertaker}~\cite{undertaker} is a tool implemented in C++ that parses
Kconfig models and does configuration analysis on them. It can check the
structure of preprocessor directives in the Linux kernel's source code against
different configuration models to find blocks of features that cannot be
selected or deselected.

\textit{LVAT}~\cite{she2010formal, lvat} (Linux Variability Analysis Tools) is
a tool suite written in Scala for analyzing Kconfig models. It currently offers
three different tools: propositional formula extractor, Kconfig statistics, and
feature model translator. It relies on a utility called
\textit{exconfig}~\cite{exconfig} (Linux Kconfig Extractor), which is
implemented in C, that utilizes Linux's Kconfig infrastructure to dump the
internal representation to an \texttt{.exconfig} file~\cite{exconfig_extracts},
which is then parsed by LVAT.

\textit{RangeFix}~\cite{xiong2014range, rangefix} is an algorithm for finding
fixes to resolve configuration conflicts. A proof-of-concept with a
command-line interface has been implemented in Scala that works with both
Kconfig and eCos' CDL files. The Kconfig version depends on LVAT, and takes an
\texttt{.exconfig} file, a \texttt{.config} configuration file, a configuration
option name and a requested value as inputs. If setting the configuration
option to the requested value results in a conflict, RangeFix returns one or
more fixes that the user can implement in her configuration to set the
configuration option to the requested value and satisfy its dependencies. The
algorithm depends on a constraint solver, and the Scala implementation utilizes
the SMT solver Z3~\cite{z3}.

\textit{Satconfig}~\cite{satconfig} is a tool for creating a valid
\texttt{.config} configuration file from a partial configuration. The user only
has to set the values of the configuration options she is interested in, and
Satconfig will automatically fill in any blanks to satisfy their dependencies.
However, if there are any conflicts, Satconfig quits since it is not able to
resolve any conflicts. Satconfig is written in C and is integrated as an
additional command in the Linux kernel's build system. It started as a Google
Summer of Code project, but it has not been merged upstream and is only
available in a downstream fork of the kernel repository~\cite{satconfig}.
Satconfig translates the Linux kernel's internal Kconfig data structure
directly into CNF clauses and appends them to the SAT solver
PicoSAT~\cite{picosat} by using its C API.

\section{RangeFix}
\label{rangefix}

RangeFix was already introduced in the previous section. However, since this
thesis bases a lot of its work on RangeFix, the algorithm needs a more detailed
explanation.

RangeFix is a novel algorithm for finding minimal fixes to a configuration that
resolve any unsatisfied constraints~\cite{xiong2014range}. Since it is just an
algorithm, it is not bound to any feature modeling language in particular.
However, the algorithm has been tested with both Kconfig and CDL through an
implementation in Scala.

The functionality of RangeFix is best illustrated by a small example, taken
from the RangeFix paper~\cite{xiong2014range}. Assume the following set of
configuration options are declared in a feature model:
$$\{m : \texttt{Bool},\ a : \texttt{Int},\ b : \texttt{Int}\}$$
Furthermore, assume that the feature model contains the following three
propositional logical constraints:
$$(m \to a > 10) \land (\lnot m \to b > 10) \land (a < b)$$
Our feature model now contains three variables (\textit{m}, \textit{a} and
\textit{b}) and three constraints over these configuration options. Continuing
with the example, assume that there is also the following configuration:
$$\{m := \texttt{true},\ a := 6,\ b := 5\}$$
Values have now been assigned to the feature model's variables. However, these
value assignments violate a couple of the constraints. More specifically, the
first and the last constraints are violated:
$$(\texttt{true} \to 6 > 10) \land (\lnot \texttt{true} \to 5 > 10) \land (6 <
5)$$
The first is violated because $(\texttt{true} \to 6 > 10) = (\texttt{true} \to
\texttt{false}) = \texttt{false}$. The third is violated because $(6 < 5) =
\texttt{false}$. Clearly, some of the variables need to have their values
changed to satisfy the constraints. By applying the RangeFix algorithm to this
problem, the following two fixes are generated:
\vspace{0.3cm}
\begin{itemize}
  \item $[m := \texttt{false},\ b : b > 10]$
  \item $[(a, b) : a > 10 \land a < b]$
\end{itemize}
\vspace{0.3cm}
All conflicts will be resolved and the configuration satisfied if one of these
two fixes is applied. In other words, the user has two options in how to
resolve the conflicts and satisfy the constraints. If the first fix is
selected, the assignment to $m$ needs to be changed from \texttt{true} to
\texttt{false}, and $b$ from $5$ to something greater than $10$. $a$ does not
need to be changed in the first fix. If the second fix is selected, both $a$
and $b$ need to be changed in such a way that $a > 10$ and $a < b$ are true.

\subsection{RangeFix's three stages}

RangeFix generates fixes in three separate steps called \textit{stages}. The
variables that need to be changed are found during the first stage. These
variables are organized in sets, called \textit{diagnoses}. Returning to the
previous example, the two located diagnoses were:
\vspace{0.3cm}
\begin{itemize}
  \item $\{m,\ b\}$
  \item $\{a,\ b\}$
\end{itemize}
\vspace{0.3cm}

In the second stage, the constraints are transformed into \textit{modified
constraints}. The transformation is done for each diagnosis by retrieving the
constraints affecting the diagnosis and replacing all variables in the
constraints that are not part of the diagnosis with the configuration's value.
Continuing with the running example, the two modified constraints were:
\vspace{0.3cm}
\begin{itemize}
  \item $(m \to 6 > 10) \land (\lnot m \to b > 10) \land (6 < b)$
  \item $(\texttt{true} \to a > 10) \land (\lnot \texttt{true} \to b > 10)
    \land (a < b)$
\end{itemize}
\vspace{0.3cm}
In the first modified constraint, the applied diagnosis was $\{m,\ b\}$, and
all other configuration options have therefore been substituted with their
configuration value; in this case have all occurrences of $a$ been substituted
with $6$. In the second modified constraint have all occurrences of $m$ been
substituted with \texttt{true}.

The modified constraints are minimized into fixes during the last, and final,
stage of RangeFix. The minimization is achieved through a process where several
heuristic rules are applied to rewrite the modified constraints into simpler
units. The end result of this stage are the fixes:
\vspace{0.3cm}
\begin{itemize}
  \item $[m := \texttt{false},\ b : b > 10]$
  \item $[(a, b) : a > 10 \land a < b]$
\end{itemize}
\vspace{0.3cm}

\subsection{Generating diagnoses}

A diagnosis is
\begin{itemize}

  \item a subset of configuration options that are violating one or more of the
    constraints, and is

  \item able to satisfy the constraints when the values of its configuration
    options are changed properly.

\end{itemize}
An important property of a diagnosis is that each configuration option in the
diagnosis is part of an unsatisfiable core. Furthermore, for a diagnosis to be
able to satisfy the configuration, it must contain at least one configuration
option from each unsatisfiable core. To be able to understand how this works, a
closer look at hard and soft constraints in conjunction with a satisfiability
solver is needed.

Returning to the running example for this section, the constraints were:
$$(m \to a > 10) \land (\lnot m \to b > 10) \land (a < b)$$
These are considered to be hard constraints, because lessening them is not an
option when seeking for a valid configuration. Continuing on, there was also
the following configuration:
$$\{m := \texttt{true},\ a := 6,\ b := 5\}$$
These assignments are the soft constraints. It would be best if as many
assignments as possible could be made, but if that is not possible due to
constraint violations, some need to be edited.

Constraint solvers often only understand the concepts of variables and
constraints, and there is usually no distinction made between hard and soft
constraints. However, assuming that there is at least one assignment of the
configuration options that do not cause any constraint violations, each
unsatisfiable core must contain at least one soft constraint. Because it is
only from the user's configuration choices that conflicts can be introduced. In
the running example, the union of the constraints are:
$$(m \to a > 10) \land (\lnot m \to b > 10) \land (a < b) \land (m :=
\texttt{true}) \land (a := 6) \land (b := 5)$$
The minimal unsatisfiable cores are:
\vspace{0.3cm}
\begin{itemize}

  \item $(m \to a > 10) \land (m := \texttt{true}) \land (a := 6)$

  \item $(a < b) \land (a := 6) \land (b := 5)$

  \item $(m \to a > 10) \land (\lnot m \to b > 10) \land (m := \texttt{true})
    \land (b := 5)$

  \item $(m \to a > 10) \land (\lnot m \to b > 10) \land (a := 6) \land (b :=
    5)$

  \item $(m \to a > 10) \land (\lnot m \to b > 10) \land (a < b) \land (b :=
    5)$

\end{itemize}
\vspace{0.3cm}
A diagnosis contains at least one soft constraint from each minimal
unsatisfiable core. By selecting either $(m := \texttt{true}) \land (b := 5)$
or $(a := 6) \land (b := 5)$, all unsatisfiable cores are covered. In other
words, by changing the values of either $\{m,\ b\}$ or $\{a,\ b\}$, it is
possible to satisfy the constraints.

In Figure~\ref{flowchart}, a visualization of the first stage of the RangeFix
algorithm can be seen on a high level in the form of a flowchart. Starting with
an empty partial diagnosis, the algorithm enters a loop where it tries to
expand the partial diagnosis into more partial diagnoses and make them as large
as possible. When a partial diagnosis does not cause any more unsatisfiable
cores to be detected, it is appended to the set $R$ as a diagnosis. The
important part to note here is that the constraint solver has to be invoked
during each iteration to tell whether the partial diagnosis is large enough to
eliminate all unsatisfiable cores; if not, it will extract an unsatisfiable
core to construct one or more larger partial diagnoses.

\begin{figure}[h!]
  \centering
  \includegraphics[width=0.5\textwidth]{images/flowchart.png}
  \caption{A flowchart of the first stage of the RangeFix algorithm on a high
  level.}
  \label{flowchart}
\end{figure}

\subsection{Encode a Kconfig model as an SMT problem}

The Scala implementation of RangeFix utilizes an SMT solver to find and extract
unsatisfiable cores. A brief overview of how a Kconfig model is encoded as an
SMT problem is given in this section.

Boolean variables are encoded with SMT's primitive type \texttt{Bool}, while
tristates are encoded with an enum with three states. The enum is called
\texttt{\_\_enum\_\_0}, and its definition is shown in Listing~\ref{fig:smt_1}.

\begin{figure}
  \begin{lstlisting}[
    caption={The tristate type declared with an enum.},
    label=fig:smt_1,
  ]
(declare-datatypes () ((__enum__0
  __s__enum_int__0_0 __s__enum_int__0_1 __s__enum_int__0_2)))
  \end{lstlisting}
\end{figure}

For each configuration option that appears in the Kconfig model, two SMT
variables are allocated. The first variable contains the configuration option's
value, and its type maps to its corresponding type in the Kconfig model. For
instance, a \texttt{tristate} in the Kconfig model maps to a
\texttt{\_\_enum\_\_0} in SMT, while a \texttt{bool} in the Kconfig model maps
to a \texttt{Bool} in SMT. The second variable is a \texttt{Bool}, regardless
of the configuration option's type, and it is used to determine the first
variable's value source. If the second variable is set to \texttt{True}, the
first variable is assigned to what is declared in the \texttt{.config} file. If
the second variable is instead set to \texttt{False}, the SMT solver is free to
determine the value of the first variable.

An example of a Kconfig model, a \texttt{.config} file and the corresponding
SMT variables are shown in Listing~\ref{fig:smt_2_1}, Listing~\ref{fig:smt_2_2}
and Listing~\ref{fig:smt_2_3}. In Listing~\ref{fig:smt_2_3}, the SMT solver
will use the values from the \texttt{.config} file when \texttt{\_\_gd\_\_A}
and \texttt{\_\_gd\_\_B} are set to \texttt{True}. This is done with the two
assert commands in Listing~\ref{fig:smt_3}, which also name the variables to be
able to extract the unsatisfiable cores that the configuration might give rise
to.

\begin{figure}
  \begin{subfigure}[t]{0.4\textwidth}
    \begin{lstlisting}[
      caption={A Kconfig model with exconfig's syntax.},
      label={fig:smt_2_1},
    ]
config A tristate {
}
config B boolean {
}
    \end{lstlisting}
  \end{subfigure}
  \hfill
  \begin{subfigure}[t]{0.5\textwidth}
    \begin{lstlisting}[
      caption={A corresponding \texttt{.config} configuration file.},
      label={fig:smt_2_2},
    ]
CONFIG_A=n
CONFIG_C=n
    \end{lstlisting}
  \end{subfigure}
  \begin{subfigure}[t]{\textwidth}
    \begin{lstlisting}[
      caption={Allocated SMT variables from Listing~\ref{fig:smt_2_1}, and
      assert commmands to control their value from and
      Listing~\ref{fig:smt_2_2}.}, label={fig:smt_2_3},
    ]
(declare-const A __enum__0)
(declare-const B Bool)
(declare-const __gd__A Bool)
(declare-const __gd__B Bool)
(assert (or (not __gd__A) (= A __s__enum_int__0_0)))
(assert (or (not __gd__B) (= B false)))
    \end{lstlisting}
  \end{subfigure}
\end{figure}

\begin{figure}
  \begin{lstlisting}[
    caption={The allocated SMT variables and the asserts to control their
    value source.},
    label={fig:smt_3},
  ]
(assert (! __gd__A :named __ex____gd__A))
(assert (! __gd__B :named __ex____gd__B))
  \end{lstlisting}
\end{figure}

The constraints are declared using a set of various functions. A minimal
example for a configuration option \texttt{C} that has no dependencies is shown
in Listing~\ref{fig:smt_4}. This small example illustrates how the Scala
implementation of RangeFix creates functions for each configuration option that
defines its lower and upper bounds. At the last line, an assert is made to set
\texttt{C}'s value to \texttt{yes} by specifying that the function
\texttt{C\_\_effective}'s return value should be equal to
\texttt{\_\_s\_\_enum\_int\_\_0\_2}.

\begin{figure}
  \begin{lstlisting}[
    caption={An example of SMT functions.},
    label={fig:smt_4},
  ]
(define-fun C__upperBound () Bool true)
(define-fun C__inherited () __enum__0 __s__enum_int__0_2)
(define-fun C__lowerBound () Bool false)
(define-fun C__rangedUserValue () Bool
  (or (and C C__upperBound) C__lowerBound))
(define-fun C__default () Bool
  (or (and false true) C__lowerBound))
(define-fun C__effective () Bool
  (ite (= C__inherited __s__enum_int__0_0)
    C__default C__rangedUserValue))
(assert (= __s__enum_int__0_2 C__effective))
  \end{lstlisting}
\end{figure}

\section{Related works}

\paragraph{Usability of the Linux configurators.}

Improving the usability of the Linux kernel configurators has been studied by
Bak et al.~\cite{bakimproving}. In their study, they had identified the
usability to be lacking, and used xconfig as an example to highlight these
usability problems. They implemented a prototype that they called lkc, which
would eliminate many of their identified usability problems. lkc was evaluated
with users, and they received positive feedback saying that their changes were
definitely an improvement over xconfig. Among the users' desired additional
features was conflict-resolution.

\paragraph{Formalizing the Kconfig language.}

Since the Kconfig language lacks a formal specification, semantics of it have
been described by studying the behavior exhibited in
xconfig~\cite{she2010formal}. An experiment to formalize the language, by
defining propositional formulas that describe all valid configurations of the
Linux kernel, has also been made~\cite{zengler2010encoding, walch2015formal},
where implementing it with SAT solvers is highlighted as a possible
application.

\paragraph{Resolving configuration conflicts.}

As mentioned in Section~\ref{overview_kconfig_tools}, there are several
projects that have implemented various reasoners for Kconfig feature models.
One of them was RangeFix~\cite{xiong2014range}, which is an algorithm for
resolving configuration conflicts. It was also mentioned that RangeFix has a
Scala implementation that works with Kconfig feature models.

\paragraph{Feature model analysis.}

Through a variety of papers, different aspects of the Linux kernel's feature
model have been studied. One such aspect is the evolution of the kernel's
feature model, which involves the growth of the number of
features~\cite{lotufo2010evolution, koren2006study}. Another aspect is finding
zombie features, which are bugs where features cannot be either enabled or
disabled at all~\cite{tartler2009dead}. How features are scattered in a
non-modular way has also been researched~\cite{passos2015feature}.

\paragraph{Knowledge based configurators.}

Much work has been done for configurators for physical goods in the field of
knowledge-based configuration, a subfield of AI, but relatively little research
has been done in the area of configurators for
software~\cite{hubaux2012unifying}. In manufacturing, companies adopt
configurators to gain commercial advantages such as flexibility, lower
production costs and increased customer satisfaciton~\cite{hubaux2012feature}.
These are built on models called product variant master, which is a set of
hierarchically organised components that aims at capturing variability of the
product range~\cite{hubaux2012feature}. Abbasi et al.~\cite{abbasi2012s} did a
study on 111 web-based product configurators and found that one can do a lot of
parallels to software configurators. Since there exists a big overlap between
the fields, an attempt at unifying these two fields has recently been
made~\cite{hubaux2012unifying}.
