\chapter{Methodology}
\label{methodology}

The methodology applied in this case study has been the one described by design
science research~\cite{peffers2006design}. Design science research is suitable
for development projects and it prescribes a process of six activities. The six
activities are \begin{enumerate*} \item \textit{problem identification and
motivation}, \item \textit{objectives of a solution}, \item \textit{design and
development}, \item \textit{demonstration}, \item \textit{evaluation}, and
\item \textit{communication}.\end{enumerate*} How these activities have been
applied in this case study is expanded upon in the following sections in this
chapter.

\section{Problem identification and motivation}

In Section~\ref{motivation}, the relevancy of improving the configuration
process of Kconfig feature models was argued for. Statistics from a user survey
were presented, telling us that the configurators were indeed lacking in
usability. Confirming the issues were kernel developers, who have started an
initiative called kconfig-sat, where the aim is to add support for automatic
dependency-resolution. From the gathered evidence, it is apparent that it would
be beneficial if people who configure Kconfig based software could receive aid
from the configurator with resolving unmet dependencies.

\section{Objectives of a solution}

With the problem having been identified, the next step was to formulate an
objective on how to best address it. In Section~\ref{featuremodeling}, we
presented examples of other feature modeling tools similar to the Linux
kernel's configurator xconfig. Two of those were the eCos Configuration Tool
and pure::variants, both of which have built-in support for conflict-resolution
assistance. Using these solutions as an inspiration, implementing a similar
solution for the Linux kernel seemed like a reasonable way to address the
issue. We also noted in Section~\ref{motivation} that there are Linux users who
have also arrived at the same conclusion---that adding support in the Kconfig
configurators for resolving dependencies would be beneficial. The objective
therefore became to implement an interactive conflict-resolution tool that
integrates with one of the Kconfig configurators. Whenever the user attempts to
enable a configuration option with unmet dependencies, the configurator could
present a list of possible fixes that would resolve the unmet dependencies.

\section{Design and development}

The design and development activity was initiated by an investigation of
available tools related to Kconfig. An overview of the tools that were
investigated is presented in Section~\ref{overview_kconfig_tools}. Among these,
the ones that appeared as having the highest potential in achieving the goal of
the research questions were RangeFix and Satconfig. RangeFix is an algorithm
for resolving configuration conflicts, which has an existing Scala
implementation that works with Kconfig. Satconfig is tool for automatically
completing a partial \texttt{.config} configuration file, that is implemented
in C, and contains a mechanism for translating a Kconfig model into a SAT
problem. By combining these projects, the plan was to realize a C
implementation of RangeFix that utilizes Satconfig's translation of Kconfig
models into SAT problems. To make the conflict-resolution process interactive,
the implementation of GUI additions in one of the Kconfig configurators to
support such a workflow also had to be achieved.

The existing Scala implementation of RangeFix utilizes an SMT solver for
encoding the Kconfig model and detecting unsatisfiable cores. Since SMT
provides a richer modeling language than SAT, it is therefore more
straightforward to model Kconfig constraints that involve strings and integers.
Another strength with SMT in our context is that the Scala implementation of
RangeFix has already been implemented using SMT. On the other hand, only
$6\,\%$ of the configuration options for x86 are of other types than bool and
tristate~\cite{berger2013study}, which means that for the majority of cases a
SAT solver is enough. Furthermore, the Linux community acceptance factor for
SMT is also much lower than for SAT, and many would like to see the SAT avenue
investigated first~\cite{kconfig_sat}. Reasons for preferring SAT are its
relative simplicity and its smaller code base (\textasciitilde10k lines of code
for a SAT solver compared to \textasciitilde100k lines of code for an SMT
solver)~\cite{kconfig_sat_email}. The decision was therefore made to proceed
with using a SAT solver in the C implementation of RangeFix.

After the selection of tools to use as a foundation, a prototype was
implemented where the user would be able to perform conflict-resolution
interactively. This involved modifying the Kconfig configurator xconfig with
extra GUI elements to support the workflow. It was made to call the Scala
implementation of RangeFix in the background, and print the returned fixes
inside xconfig.

Next, the implementation of RangeFix in C took place. This involved examining
the algorithm and Satconfig's translation of Kconfig models into SAT problems.
However, only the first stage of the RangeFix algorithm was implemented, which
means that it would only be able to generate diagnoses and not complete fixes.
A version of xconfig that uses the C implementation for conflict-resolution was
also developed.

Details about our implementation of RangeFix in C, based on a SAT-solver, are
shared in Chapter~\ref{design_and_development}. How our implementation of
RangeFix and the existing Scala implementation of RangeFix were integrated with
xconfig is also told in that chapter.

\section{Demonstration}

From the design and development activity, two artifacts were produced. Both
were versions of xconfig, but they were calling different implementations of
RangeFix for dependency-resolution. One was a version of xconfig for version
2.6.32 of the Linux kernel that made calls to the Scala implementation of
RangeFix for fix generation. The second was a version of xconfig for version
4.4 of the Linux kernel that generated diagnoses using our C implementation of
RangeFix. These two artifacts are demonstrated in Chapter~\ref{demonstration}.

\section{Evaluation}

The evaluation of the two produced artifacts is presented in
Chapter~\ref{evaluation}. The qualities performance, correctness and usability
were evaluated.

Through a survey with Linux kernel developers and users, this thesis' direction
was evaluated. In Section~\ref{user_survey}, the design and results of the
survey are presented. The participants were asked to fill in the survey that
can be seen in Appendix~\ref{appendix_1}. They were, among things, questioned
on the method they currently use to enable a disabled configuration option, and
the time it takes on average to accomplish that task. This established a
baseline for how fast an interactive dependency-resolution solution would need
to function. The participants were also shown a video of our version of xconfig
that calls the Scala implementation of RangeFix, and through several questions
they were asked to provide various kinds of feedback.

The Scala implementation of RangeFix was evaluated with respect to performance
and correctness. The evaluation procedure and the results are found in
Section~\ref{the_scala_implementation}. But to summarize the evaluation, a set
of disabled configuration options were randomly sampled, and for each one the
Scala implementation of RangeFix was invoked to generate fixes for enabling it.
The time from when the program was started until it returned was recorded to
determine the performance of the implementation. Furthermore, the time spent
generating the diagnoses, and converting them to fixes, was also recorded. For
each run, the correctness of the generated fixes was evaluated by testing them
in xconfig. Depending on the quality of the generated fixes in this test, one
of five different labels was given to summarize its correctness.

The C implementation of RangeFix was also evaluated with respect to performance
and correctness. The evaluation, whose procedure and results are presented in
Section~\ref{the_c_implementation}, was carried out similarly to the previous
one for the Scala implementation. However, since we were only able to implement
in C the first stage of the algorithm, that yields the diagnoses, only that
part was evaluated. As with the previous evaluation, a set of disabled
configuration options were first randomly sampled. For each configuration
option, the performance of generating the diagnoses was evaluated by measuring
the running time of the program. The correctness of the diagnoses were
evaluated by testing if it was possible to enable the configuration option by
only modifying the values of the configuration options in them. Using the same
five correctness labels as for the Scala implementation, the results were
categorized in a comparable way.

By using the numbers collected in the survey as a baseline, it was possible to
evaluate whether the Scala implementation of RangeFix performed well enough.
Furthermore, by comparing the C implementation's running time against the Scala
implementation's time spent at generating diagnoses, it was possible to
determine if implementing it in C meant a performance improvement. With the
performance and correctness statistics, it would also be possible to decide
whether continuing implementing RangeFix in C with a SAT-solver is meaningful
to explore in future projects.

\section{Communication}

This thesis is used as a medium for communicating the results from the project.
It includes implementation details, demonstration of the produced artifacts,
and evaluation of the artifacts. Furthermore, observations and ideas that might
help in future iterations of developing an interactive conflict-resolution
mechanism are shared in Chapter~\ref{towards_c}. The information has also been
shared with the Linux developers.
