.PHONY: all evince clean

all: report.pdf

report.pdf: Main.tex images/screenshot.png $(wildcard include/*)
report.pdf: /tmp/report/img1.pdf /tmp/report/img2.pdf /tmp/report/img3.pdf
report.pdf: /tmp/report/img4.pdf /tmp/report/img5.pdf /tmp/report/img6.pdf
report.pdf: images/chart1.eps images/chart2.eps images/chart3.eps
report.pdf: images/chart4.eps images/chart5.eps images/chart6.eps
report.pdf: images/chart_survey_1.eps images/chart_survey_2.eps
report.pdf: images/chart_survey_3.eps images/chart_survey_4.eps
report.pdf: /tmp/report/datatypes.eps /tmp/report/kconfig_expression.eps
	mkdir -p /tmp/report
	find . \( -name \*.tex -o -name \*.bib -o -name \*.pdf -o -name \*.eps \
		-o -name \*.png \) \
		-exec cp --parents {} /tmp/report \;
	cd /tmp/report && \
		pdflatex Main.tex && \
		biber Main && \
		pdflatex Main.tex
	mv /tmp/report/Main.pdf report.pdf

/tmp/report/%.pdf: images/%.svg
	mkdir -p /tmp/report
	inkscape -z -D --file=$< --export-pdf=$@ --export-latex

/tmp/report/%.eps: images/%.dia
	mkdir -p /tmp/report
	dia -e $@ -t eps -O /tmp/report/ $<

evince: all
	evince report.pdf

clean:
	rm -rf /tmp/report
	rm -f report.pdf
